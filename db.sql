/* 修改商品评论表 新增pics,anonymity*（0-不匿名,1-匿名） */
CREATE TABLE IF NOT EXISTS `yp_comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `goodid` int(100) unsigned NOT NULL DEFAULT '0',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `score` int(5) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `goodscore` int(5) unsigned NOT NULL DEFAULT '0',
  `servicescore` int(5) unsigned NOT NULL DEFAULT '0',
  `deliveryscore` int(5) unsigned NOT NULL DEFAULT '0',
  `content` varchar(225) DEFAULT NULL,
  `tag` varchar(225) DEFAULT NULL,
  `pics` varchar(225) NOT NULL DEFAULT '' COMMENT '图片',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '1-可见 -1-无效',
  `uid` int(10) unsigned NOT NULL DEFAULT '0',
  `anonymity` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '匿名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商品表';

/* 修改商品退货表  新增字段pics */
CREATE TABLE `yp_backlist` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`goodid` int(10) unsigned NOT NULL DEFAULT '0',
`num` int(10) unsigned NOT NULL DEFAULT '0',
`tool` varchar(225) NOT NULL DEFAULT '' COMMENT '订单号',
`toolid` varchar(225) NOT NULL DEFAULT '' COMMENT '订单号',
`uid` int(10) NOT NULL DEFAULT '0',
`status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '数据状态',
`create_time` int(10) unsigned NOT NULL DEFAULT '0',
`info` varchar(225) NOT NULL DEFAULT '',
`total` decimal(50,2) NOT NULL DEFAULT '0.00',
`backinfo` varchar(225) NOT NULL DEFAULT '',
`address` varchar(225) NOT NULL DEFAULT '',
`update_time` int(10) unsigned NOT NULL DEFAULT '0',
`parameters` varchar(225) NOT NULL DEFAULT '',
`assistant` int(10) unsigned NOT NULL DEFAULT '0',
`shopid` int(10) unsigned NOT NULL DEFAULT '0',
`title` varchar(225) NOT NULL DEFAULT '',
`reason` varchar(225) NOT NULL DEFAULT '',
`contact` varchar(225) NOT NULL DEFAULT '',
`backname` varchar(225) NOT NULL DEFAULT '',
`pics` varchar(255) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='退货表'

/* 修改商品换货表  新增字段pics */
CREATE TABLE `yp_exchange` (
`id` int(10) NOT NULL AUTO_INCREMENT,
`goodid` int(10) unsigned NOT NULL DEFAULT '0',
`num` int(10) unsigned NOT NULL DEFAULT '0',
`tool` varchar(225) NOT NULL DEFAULT '' COMMENT '订单号',
`toolid` varchar(225) NOT NULL DEFAULT '' COMMENT '订单号',
`uid` int(10) unsigned NOT NULL DEFAULT '0',
`status` tinyint(2) NOT NULL DEFAULT '0',
`create_time` int(10) unsigned NOT NULL DEFAULT '0',
`info` varchar(225) NOT NULL DEFAULT '',
`total` decimal(50,2) NOT NULL DEFAULT '0.00',
`backinfo` varchar(225) NOT NULL DEFAULT '',
`shopid` int(10) unsigned NOT NULL DEFAULT '0',
`update_time` int(10) unsigned NOT NULL DEFAULT '0',
`assistant` int(10) unsigned NOT NULL DEFAULT '0',
`title` varchar(225) NOT NULL DEFAULT '',
`reason` varchar(225) NOT NULL DEFAULT '',
`changetool` varchar(225) NOT NULL DEFAULT '',
`changetoolid` varchar(225) NOT NULL DEFAULT '',
`address` varchar(225) NOT NULL DEFAULT '',
`contact` varchar(225) NOT NULL DEFAULT '',
`parameters` varchar(225) NOT NULL DEFAULT '',
`backname` varchar(225) NOT NULL DEFAULT '',
`acceptname` varchar(255) NOT NULL DEFAULT '',
`acceptphone` varchar(225) NOT NULL DEFAULT '',
`acceptaddress` varchar(225) NOT NULL DEFAULT '',
`pics` varchar(255) DEFAULT '',
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='表'

CREATE TABLE `yp_goods` (
 `goods_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
 `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
 `goods_sn` varchar(60) NOT NULL DEFAULT '',
 `goods_name` varchar(120) NOT NULL DEFAULT '',
 `goods_name_style` varchar(60) NOT NULL DEFAULT '+',
 `click_count` int(10) unsigned NOT NULL DEFAULT '0',
 `brand_id` smallint(5) unsigned NOT NULL DEFAULT '0',
 `provider_name` varchar(100) NOT NULL DEFAULT '',
 `goods_number` smallint(5) unsigned NOT NULL DEFAULT '0',
 `goods_weight` decimal(10,3) unsigned NOT NULL DEFAULT '0.000',
 `market_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
 `shop_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
 `promote_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
 `promote_start_date` int(11) unsigned NOT NULL DEFAULT '0',
 `promote_end_date` int(11) unsigned NOT NULL DEFAULT '0',
 `warn_number` tinyint(3) unsigned NOT NULL DEFAULT '1',
 `keywords` varchar(255) NOT NULL DEFAULT '',
 `goods_brief` varchar(255) NOT NULL DEFAULT '',
 `goods_desc` text NOT NULL,
 `goods_thumb` varchar(255) NOT NULL DEFAULT '',
 `goods_img` varchar(255) NOT NULL DEFAULT '',
 `original_img` varchar(255) NOT NULL DEFAULT '',
 `is_real` tinyint(3) unsigned NOT NULL DEFAULT '1',
 `extension_code` varchar(30) NOT NULL DEFAULT '',
 `is_on_sale` tinyint(1) unsigned NOT NULL DEFAULT '1',
 `is_alone_sale` tinyint(1) unsigned NOT NULL DEFAULT '1',
 `is_shipping` tinyint(1) unsigned NOT NULL DEFAULT '0',
 `integral` int(10) unsigned NOT NULL DEFAULT '0',
 `add_time` int(10) unsigned NOT NULL DEFAULT '0',
 `sort_order` smallint(4) unsigned NOT NULL DEFAULT '100',
 `is_delete` tinyint(1) unsigned NOT NULL DEFAULT '0',
 `is_best` tinyint(1) unsigned NOT NULL DEFAULT '0',
 `is_new` tinyint(1) unsigned NOT NULL DEFAULT '0',
 `is_hot` tinyint(1) unsigned NOT NULL DEFAULT '0',
 `is_promote` tinyint(1) unsigned NOT NULL DEFAULT '0',
 `bonus_type_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
 `last_update` int(10) unsigned NOT NULL DEFAULT '0',
 `goods_type` smallint(5) unsigned NOT NULL DEFAULT '0',
 `seller_note` varchar(255) NOT NULL DEFAULT '',
 `give_integral` int(11) NOT NULL DEFAULT '-1',
 `rank_integral` int(11) NOT NULL DEFAULT '-1',
 `suppliers_id` smallint(5) unsigned DEFAULT NULL,
 `is_check` tinyint(1) unsigned DEFAULT NULL,
 PRIMARY KEY (`goods_id`),
 KEY `goods_sn` (`goods_sn`),
 KEY `cat_id` (`cat_id`),
 KEY `last_update` (`last_update`),
 KEY `brand_id` (`brand_id`),
 KEY `goods_weight` (`goods_weight`),
 KEY `promote_end_date` (`promote_end_date`),
 KEY `promote_start_date` (`promote_start_date`),
 KEY `goods_number` (`goods_number`),
 KEY `sort_order` (`sort_order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商品表'

CREATE TABLE IF NOT EXISTS `yp_goods_type` (
 `cat_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
 `cat_name` varchar(60) NOT NULL DEFAULT '',
 `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
 `attr_group` varchar(255) NOT NULL,
 PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商品类型表'

CREATE TABLE IF NOT EXISTS `yp_goods_attribute` (
 `attr_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
 `cat_id` smallint(5) unsigned NOT NULL DEFAULT '0',
 `attr_name` varchar(60) NOT NULL DEFAULT '',
 `attr_input_type` tinyint(1) unsigned NOT NULL DEFAULT '1',
 `attr_type` tinyint(1) unsigned NOT NULL DEFAULT '1',
 `attr_values` text NOT NULL,
 `attr_index` tinyint(1) unsigned NOT NULL DEFAULT '0',
 `sort_order` tinyint(3) unsigned NOT NULL DEFAULT '0',
 `is_linked` tinyint(1) unsigned NOT NULL DEFAULT '0',
 `attr_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
 PRIMARY KEY (`attr_id`),
 KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商品属性表'

CREATE TABLE IF NOT EXISTS `yp_goods_attr` (
 `goods_attr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `goods_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
 `attr_id` smallint(5) unsigned NOT NULL DEFAULT '0',
 `attr_value` text NOT NULL,
 `attr_price` varchar(255) NOT NULL DEFAULT '',
 PRIMARY KEY (`goods_attr_id`),
 KEY `goods_id` (`goods_id`),
 KEY `attr_id` (`attr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='商品属性价格表'

CREATE TABLE IF NOT EXISTS `yp_attribute` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`name` varchar(30) NOT NULL DEFAULT '' COMMENT '字段名',
`title` varchar(100) NOT NULL DEFAULT '' COMMENT '字段注释',
`field` varchar(100) NOT NULL DEFAULT '' COMMENT '字段定义',
`type` varchar(20) NOT NULL DEFAULT '' COMMENT '数据类型',
`value` varchar(100) NOT NULL DEFAULT '' COMMENT '字段默认值',
`remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
`is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示',
`extra` varchar(255) NOT NULL DEFAULT '' COMMENT '参数',
`model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
`is_must` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填',
`status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
`update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
`create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
`validate_rule` varchar(255) NOT NULL DEFAULT '',
`validate_time` tinyint(1) unsigned NOT NULL DEFAULT '0',
`error_info` varchar(100) NOT NULL DEFAULT '',
`validate_type` varchar(25) NOT NULL DEFAULT '',
`auto_rule` varchar(100) NOT NULL DEFAULT '',
`auto_time` tinyint(1) unsigned NOT NULL DEFAULT '0',
`auto_type` varchar(25) NOT NULL DEFAULT '',
`issys` tinyint(1) unsigned NOT NULL DEFAULT '0',
PRIMARY KEY (`id`),
KEY `model_id` (`model_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='模型属性表'



/* 新增回复商品评论表 */
CREATE TABLE IF NOT EXISTS `yp_reply_comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0',
  `commentid` int(10) NOT NULL DEFAULT '0',
  `content` varchar(225) NOT NULL DEFAULT '' COMMENT '回复内容',
  `status` tinyint(2) NOT NULL COMMENT '默认0-未提交订单1-未读2-已读',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='回复评论表';


/* 修改退货表 yp_backlist 添加卖家发送快递名称 changetool 卖家发送快递单号 changetoolid */
CREATE TABLE IF NOT EXISTS `yp_backlist` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `goodid` int(10) unsigned NOT NULL DEFAULT '0',
  `num` int(10) unsigned NOT NULL DEFAULT '0',
  `tool` varchar(225) NOT NULL DEFAULT '' COMMENT '订单号',
  `toolid` varchar(225) NOT NULL DEFAULT '' COMMENT '订单号',
  `uid` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '数据状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `info` varchar(225) NOT NULL DEFAULT '',
  `total` decimal(50,2) NOT NULL DEFAULT '0.00',
  `backinfo` varchar(225) NOT NULL DEFAULT '',
  `address` varchar(225) NOT NULL DEFAULT '',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `parameters` varchar(225) NOT NULL DEFAULT '',
  `assistant` int(10) unsigned NOT NULL DEFAULT '0',
  `shopid` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(225) NOT NULL DEFAULT '',
  `reason` varchar(225) NOT NULL DEFAULT '',
  `contact` varchar(225) NOT NULL DEFAULT '',
  `backname` varchar(225) NOT NULL DEFAULT '',
  `changetool` varchar(225) NOT NULL DEFAULT '',
  `changetoolid` varchar(225) NOT NULL DEFAULT '',
  `pics` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='退货表';


/*jqueryzoom&&jcarousel*/
(function($){
$.fn.jqueryzoom=function(options){
var settings={
xzoom:200,
yzoom:200,
offset:10,
position:"right",
lens:1,
preload:1};
if(options){
$.extend(settings,options);}
var noalt='';
$(this).hover(function(){
var imageLeft=$(this).offset().left;
var imageTop=$(this).offset().top;
var imageWidth=$(this).children('img').get(0).offsetWidth;
var imageHeight=$(this).children('img').get(0).offsetHeight;
noalt=$(this).children("img").attr("alt");
var bigimage=$(this).children("img").attr("jqimg");
$(this).children("img").attr("alt",'');
if($("div.zoomdiv").get().length==0){
$(this).after("<div class='zoomdiv'><img class='bigimg' src='"+bigimage+"'/></div>");
$(this).append("<div class='jqZoomPup'>&nbsp;</div>");}
if(settings.position=="right"){
if(imageLeft+imageWidth+settings.offset+settings.xzoom>screen.width){
leftpos=imageLeft-settings.offset-settings.xzoom;}else{
leftpos=imageLeft+imageWidth+settings.offset;}}else{
leftpos=imageLeft-settings.xzoom-settings.offset;
if(leftpos<0){
leftpos=imageLeft+imageWidth+settings.offset;}}
$("div.zoomdiv").css({top:imageTop,left:leftpos});
$("div.zoomdiv").width(settings.xzoom);
$("div.zoomdiv").height(settings.yzoom);
$("div.zoomdiv").show();
if(!settings.lens){
$(this).css('cursor','crosshair');}
$(document.body).mousemove(function(e){
mouse=new MouseEvent(e);
var bigwidth=$(".bigimg").get(0).offsetWidth;
var bigheight=$(".bigimg").get(0).offsetHeight;
var scaley='x';
var scalex='y';
if(isNaN(scalex)|isNaN(scaley)){
var scalex=(bigwidth/imageWidth);
var scaley=(bigheight/imageHeight);
$("div.jqZoomPup").width((settings.xzoom)/(scalex*1));
$("div.jqZoomPup").height((settings.yzoom)/(scaley*1));
if(settings.lens){
$("div.jqZoomPup").css('visibility','visible');}}
xpos=mouse.x-$("div.jqZoomPup").width()/2-imageLeft;
ypos=mouse.y-$("div.jqZoomPup").height()/2-imageTop;
if(settings.lens){
xpos=(mouse.x-$("div.jqZoomPup").width()/2 < imageLeft ) ? 0 : (mouse.x + $("div.jqZoomPup").width()/2>imageWidth+imageLeft)?(imageWidth-$("div.jqZoomPup").width()-2):xpos;
ypos=(mouse.y-$("div.jqZoomPup").height()/2 < imageTop ) ? 0 : (mouse.y + $("div.jqZoomPup").height()/2>imageHeight+imageTop)?(imageHeight-$("div.jqZoomPup").height()-2):ypos;}
if(settings.lens){
$("div.jqZoomPup").css({top:ypos,left:xpos});}
scrolly=ypos;
$("div.zoomdiv").get(0).scrollTop=scrolly*scaley;
scrollx=xpos;
$("div.zoomdiv").get(0).scrollLeft=(scrollx)*scalex;});},function(){
$(this).children("img").attr("alt",noalt);
$(document.body).unbind("mousemove");
if(settings.lens){
$("div.jqZoomPup").remove();}
$("div.zoomdiv").remove();});
count=0;
if(settings.preload){}}})(jQuery);
function MouseEvent(e){
this.x=e.pageX;
this.y=e.pageY;}

/*utility by springChun*/
Function.prototype.overwrite=function(f){
var result=f;
if(!result.original){
result.original=this;}
return result;}
Date.prototype.toString=Date.prototype.toString.overwrite(
function(format){
var result=new String();
if(typeof(format)=="string"){
result=format;
result=result.replace(/yyyy|YYYY/,this.getFullYear());
result=result.replace(/yy|YY/,this.getFullYear().toString().substr(2,2));
result=result.replace(/MM/,this.getMonth()>=9?this.getMonth()+1:"0"+(this.getMonth()+1));
result=result.replace(/M/,this.getMonth());
result=result.replace(/dd|DD/,this.getDate()>9?this.getDate():"0"+this.getDate());
result=result.replace(/d|D/,this.getDate());
result=result.replace(/hh|HH/,this.getHours()>9?this.getHours():"0"+this.getHours());
result=result.replace(/h|H/,this.getHours());
result=result.replace(/mm/,this.getMinutes()>9?this.getMinutes():"0"+this.getMinutes());
result=result.replace(/m/,this.getMinutes());
result=result.replace(/ss|SS/,this.getSeconds()>9?this.getSeconds():"0"+this.getSeconds());
result=result.replace(/s|S/,this.getSeconds());}
return result;});
String.prototype.format=function(){
var result=this;
if(arguments.length>0){
parameters=$.makeArray(arguments);
$.each(
parameters,
function(i,n){
result=result.replace(new RegExp("\\{"+i+"\\}","g"),n);});}
return result;}
function StringBuilder(){
this.strings=new Array();
this.length=0;}
StringBuilder.prototype.append=function(string){
this.strings.push(string);
this.length+=string.length;}
StringBuilder.prototype.toString=function(start,length){
return this.strings.join("").substr(start,length);}
/*login*/
$.login=function(options){options=$.extend({loginService:"http://passport.360buy.com/loginservice.aspx?callback=?",loginMethod:"Login",loginUrl:"https://passport.360buy.com/new/login.aspx",returnUrl:location.href,automatic:true,complete:null},options||{});if(options.loginService!=""&&options.loginMethod!=""){$.getJSON(options.loginService,{method:options.loginMethod},function(result){if(result!=null){if(options.complete!=null){options.complete(result.Identity)}if(!result.Identity.IsAuthenticated&&options.automatic&&options.loginUrl!=""){location.href=options.loginUrl+"?ReturnUrl="+escape(options.returnUrl)}}})}};
/*jmsajax*/
(function($){$.jmsajax=function(options){var defaults={type:"POST",dataType:"msjson",data:{},beforeSend:function(xhr){xhr.setRequestHeader("Content-type","application/json; charset=utf-8");},contentType:"application/json; charset=utf-8",error:function(x,s,m){alert("Status: "+((x.statusText)?x.statusText:"Unknown")+"\nMessage: "+msJSON.parse(((x.responseText)?x.responseText:"Unknown")).Message);}};var options=$.extend(defaults,options);if(options.method)
options.url+="/"+options.method;if(options.data){if(options.type=="GET"){var data="";for(var i in options.data){if(data!="")
data+="&";data+=i+"="+msJSON.stringify(options.data[i]);}
options.url+="?"+data;data=null;options.data="{}";}
else if(options.type=="POST"){options.data=msJSON.stringify(options.data);}}
if(options.success){if(options.dataType){if(options.dataType=="msjson"){var base=options.success;options.success=function(response,status){var y=dateparse(response);if(options.version){if(options.version>=3.5)
y=y.d;}
else{if(response.indexOf("{\"d\":")==0)
y=y.d;}
base(y,status);}}}}
return $.ajax(options);};dateparse=function(data){try{return msJSON.parse(data,function(key,value){var a;if(typeof value==="string"){if(value.indexOf("Date")>=0){a=/^\/Date\(([0-9]+)\)\/$/.exec(value);if(a){return new Date(parseInt(a[1],10));}}}
return value;});}
catch(e){return null;}}
msJSON=function(){function f(n){return n<10?'0'+n:n;}
var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapeable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'},rep;function quote(string){escapeable.lastIndex=0;return escapeable.test(string)?'"'+string.replace(escapeable,function(a){var c=meta[a];if(typeof c==='string'){return c;}
return'\\u'+('0000'+(+(a.charCodeAt(0))).toString(16)).slice(-4);})+'"':'"'+string+'"';}
function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];if(value&&typeof value==='object'&&typeof value.toJSON==='function'){value=value.toJSON(key);}
if(typeof rep==='function'){value=rep.call(holder,key,value);}
switch(typeof value){case'string':return quote(value);case'number':return isFinite(value)?String(value):'null';case'boolean':case'null':return String(value);case'object':if(!value){return'null';}
if(value.toUTCString){return '"\\/Date('+(value.getTime())+')\\/"';}
gap+=indent;partial=[];if(typeof value.length==='number'&&!(value.propertyIsEnumerable('length'))){length=value.length;for(i=0;i<length;i+=1){partial[i]=str(i,value)||'null';}
v=partial.length===0?'[]':gap?'[\n'+gap+
partial.join(',\n'+gap)+'\n'+
mind+']':'['+partial.join(',')+']';gap=mind;return v;}
if(rep&&typeof rep==='object'){length=rep.length;for(i=0;i<length;i+=1){k=rep[i];if(typeof k==='string'){v=str(k,value,rep);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}else{for(k in value){if(Object.hasOwnProperty.call(value,k)){v=str(k,value,rep);if(v){partial.push(quote(k)+(gap?': ':':')+v);}}}}
v=partial.length===0?'{}':gap?'{\n'+gap+partial.join(',\n'+gap)+'\n'+
mind+'}':'{'+partial.join(',')+'}';gap=mind;return v;}}
return{stringify:function(value,replacer,space){var i;gap='';indent='';if(typeof space==='number'){for(i=0;i<space;i+=1){indent+=' ';}}else if(typeof space==='string'){indent=space;}
rep=replacer;if(replacer&&typeof replacer!=='function'&&(typeof replacer!=='object'||typeof replacer.length!=='number')){throw new Error('JSON.stringify');}
return str('',{'':value});},parse:function(text,reviver){var j;function walk(holder,key){var k,v,value=holder[key];if(value&&typeof value==='object'){for(k in value){if(Object.hasOwnProperty.call(value,k)){v=walk(value,k);if(v!==undefined){value[k]=v;}else{delete value[k];}}}}
return reviver.call(holder,key,value);}
cx.lastIndex=0;if(cx.test(text)){text=text.replace(cx,function(a){return'\\u'+('0000'+(+(a.charCodeAt(0))).toString(16)).slice(-4);});}
if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,'@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']').replace(/(?:^|:|,)(?:\s*\[)+/g,''))){j=eval('('+text+')');return typeof reviver==='function'?walk({'':j},''):j;}
throw new SyntaxError('JSON.parse');}};}();})(jQuery);
/*trimpath*/
var TrimPath;(function(){
if(TrimPath==null)
TrimPath=new Object();
if(TrimPath.evalEx==null)
TrimPath.evalEx=function(src){return eval(src);};
var UNDEFINED;
if(Array.prototype.pop==null)
Array.prototype.pop=function(){
if(this.length===0){return UNDEFINED;}
return this[--this.length];};
if(Array.prototype.push==null)
Array.prototype.push=function(){
for(var i=0;i<arguments.length;++i){this[this.length]=arguments[i];}
return this.length;};
TrimPath.parseTemplate=function(tmplContent,optTmplName,optEtc){
if(optEtc==null)
optEtc=TrimPath.parseTemplate_etc;
var funcSrc=parse(tmplContent,optTmplName,optEtc);
var func=TrimPath.evalEx(funcSrc,optTmplName,1);
if(func!=null)
return new optEtc.Template(optTmplName,tmplContent,funcSrc,func,optEtc);
return null;}
try{
String.prototype.process=function(context,optFlags){
var template=TrimPath.parseTemplate(this,null);
if(template!=null)
return template.process(context,optFlags);
return this;}}catch(e){}
TrimPath.parseTemplate_etc={};
TrimPath.parseTemplate_etc.statementTag="forelse|for|if|elseif|else|var|macro";
TrimPath.parseTemplate_etc.statementDef={
"if":{delta:1,prefix:"if (",suffix:") {",paramMin:1},
"else":{delta:0,prefix:"} else {"},
"elseif":{delta:0,prefix:"} else if (",suffix:") {",paramDefault:"true"},
"/if":{delta:-1,prefix:"}"},
"for":{delta:1,paramMin:3,
prefixFunc:function(stmtParts,state,tmplName,etc){
if(stmtParts[2]!="in")
throw new etc.ParseError(tmplName,state.line,"bad for loop statement: "+stmtParts.join(' '));
var iterVar=stmtParts[1];
var listVar="__LIST__"+iterVar;
return["var ",listVar," = ",stmtParts[3],";",
"var __LENGTH_STACK__;",
"if (typeof(__LENGTH_STACK__) == 'undefined' || !__LENGTH_STACK__.length) __LENGTH_STACK__ = new Array();",
"__LENGTH_STACK__[__LENGTH_STACK__.length] = 0;",
"if ((",listVar,") != null) { ",
"var ",iterVar,"_ct = 0;",
"for (var ",iterVar,"_index in ",listVar,") { ",
iterVar,"_ct++;",
"if (typeof(",listVar,"[",iterVar,"_index]) == 'function') {continue;}",
"__LENGTH_STACK__[__LENGTH_STACK__.length - 1]++;",
"var ",iterVar," = ",listVar,"[",iterVar,"_index];"].join("");}},
"forelse":{delta:0,prefix:"} } if (__LENGTH_STACK__[__LENGTH_STACK__.length - 1] == 0) { if (",suffix:") {",paramDefault:"true"},
"/for":{delta:-1,prefix:"} }; delete __LENGTH_STACK__[__LENGTH_STACK__.length - 1];"},
"var":{delta:0,prefix:"var ",suffix:";"},
"macro":{delta:1,
prefixFunc:function(stmtParts,state,tmplName,etc){
var macroName=stmtParts[1].split('(')[0];
return["var ",macroName," = function",
stmtParts.slice(1).join(' ').substring(macroName.length),
"{ var _OUT_arr = []; var _OUT = { write: function(m) { if (m) _OUT_arr.push(m); } }; "].join('');}},
"/macro":{delta:-1,prefix:" return _OUT_arr.join(''); };"}}
TrimPath.parseTemplate_etc.modifierDef={
"eat":function(v){return "";},
"escape":function(s){return String(s).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");},
"capitalize":function(s){return String(s).toUpperCase();},
"default":function(s,d){return s!=null?s:d;}}
TrimPath.parseTemplate_etc.modifierDef.h=TrimPath.parseTemplate_etc.modifierDef.escape;
TrimPath.parseTemplate_etc.Template=function(tmplName,tmplContent,funcSrc,func,etc){
this.process=function(context,flags){
if(context==null)
context={};
if(context._MODIFIERS==null)
context._MODIFIERS={};
if(context.defined==null)
context.defined=function(str){return(context[str]!=undefined);};
for(var k in etc.modifierDef){
if(context._MODIFIERS[k]==null)
context._MODIFIERS[k]=etc.modifierDef[k];}
if(flags==null)
flags={};
var resultArr=[];
var resultOut={write:function(m){resultArr.push(m);}};
try{
func(resultOut,context,flags);}catch(e){
if(flags.throwExceptions==true)
throw e;
var result=new String(resultArr.join("")+"[ERROR: "+e.toString()+(e.message?'; '+e.message:'')+"]");
result["exception"]=e;
return result;}
return resultArr.join("");}
this.name=tmplName;
this.source=tmplContent;
this.sourceFunc=funcSrc;
this.toString=function(){return "TrimPath.Template ["+tmplName+"]";}}
TrimPath.parseTemplate_etc.ParseError=function(name,line,message){
this.name=name;
this.line=line;
this.message=message;}
TrimPath.parseTemplate_etc.ParseError.prototype.toString=function(){
return("TrimPath template ParseError in "+this.name+": line "+this.line+", "+this.message);}
var parse=function(body,tmplName,etc){
body=cleanWhiteSpace(body);
var funcText=["var TrimPath_Template_TEMP = function(_OUT, _CONTEXT, _FLAGS) { with (_CONTEXT) {"];
var state={stack:[],line:1};
var endStmtPrev=-1;
while(endStmtPrev+1<body.length){
var begStmt=endStmtPrev;
begStmt=body.indexOf("{",begStmt+1);
while(begStmt>=0){
var endStmt=body.indexOf('}',begStmt+1);
var stmt=body.substring(begStmt,endStmt);
var blockrx=stmt.match(/^\{(cdata|minify|eval)/);
if(blockrx){
var blockType=blockrx[1];
var blockMarkerBeg=begStmt+blockType.length+1;
var blockMarkerEnd=body.indexOf('}',blockMarkerBeg);
if(blockMarkerEnd>=0){
var blockMarker;
if(blockMarkerEnd-blockMarkerBeg<=0){
blockMarker="{/"+blockType+"}";}else{
blockMarker=body.substring(blockMarkerBeg+1,blockMarkerEnd);}
var blockEnd=body.indexOf(blockMarker,blockMarkerEnd+1);
if(blockEnd>=0){
emitSectionText(body.substring(endStmtPrev+1,begStmt),funcText);
var blockText=body.substring(blockMarkerEnd+1,blockEnd);
if(blockType=='cdata'){
emitText(blockText,funcText);}else if(blockType=='minify'){
emitText(scrubWhiteSpace(blockText),funcText);}else if(blockType=='eval'){
if(blockText!=null&&blockText.length>0)
funcText.push('_OUT.write( (function() { '+blockText+' })() );');}
begStmt=endStmtPrev=blockEnd+blockMarker.length-1;}}}else if(body.charAt(begStmt-1)!='$'&&
body.charAt(begStmt-1)!='\\'){
var offset=(body.charAt(begStmt+1)=='/'?2:1);
if(body.substring(begStmt+offset,begStmt+10+offset).search(TrimPath.parseTemplate_etc.statementTag)==0)
break;}
begStmt=body.indexOf("{",begStmt+1);}
if(begStmt<0)
break;
var endStmt=body.indexOf("}",begStmt+1);
if(endStmt<0)
break;
emitSectionText(body.substring(endStmtPrev+1,begStmt),funcText);
emitStatement(body.substring(begStmt,endStmt+1),state,funcText,tmplName,etc);
endStmtPrev=endStmt;}
emitSectionText(body.substring(endStmtPrev+1),funcText);
if(state.stack.length!=0)
throw new etc.ParseError(tmplName,state.line,"unclosed, unmatched statement(s): "+state.stack.join(","));
funcText.push("}}; TrimPath_Template_TEMP");
return funcText.join("");}
var emitStatement=function(stmtStr,state,funcText,tmplName,etc){
var parts=stmtStr.slice(1,-1).split(' ');
var stmt=etc.statementDef[parts[0]];
if(stmt==null){
emitSectionText(stmtStr,funcText);
return;}
if(stmt.delta<0){
if(state.stack.length<=0)
throw new etc.ParseError(tmplName,state.line,"close tag does not match any previous statement: "+stmtStr);
state.stack.pop();}
if(stmt.delta>0)
state.stack.push(stmtStr);
if(stmt.paramMin!=null&&
stmt.paramMin>=parts.length)
throw new etc.ParseError(tmplName,state.line,"statement needs more parameters: "+stmtStr);
if(stmt.prefixFunc!=null)
funcText.push(stmt.prefixFunc(parts,state,tmplName,etc));
else
funcText.push(stmt.prefix);
if(stmt.suffix!=null){
if(parts.length<=1){
if(stmt.paramDefault!=null)
funcText.push(stmt.paramDefault);}else{
for(var i=1;i<parts.length;i++){
if(i>1)
funcText.push(' ');
funcText.push(parts[i]);}}
funcText.push(stmt.suffix);}}
var emitSectionText=function(text,funcText){
if(text.length<=0)
return;
var nlPrefix=0;
var nlSuffix=text.length-1;
while(nlPrefix<text.length&&(text.charAt(nlPrefix)=='\n'))
nlPrefix++;
while(nlSuffix>=0&&(text.charAt(nlSuffix)==' '||text.charAt(nlSuffix)=='\t'))
nlSuffix--;
if(nlSuffix<nlPrefix)
nlSuffix=nlPrefix;
if(nlPrefix>0){
funcText.push('if (_FLAGS.keepWhitespace == true) _OUT.write("');
var s=text.substring(0,nlPrefix).replace('\n','\\n');
if(s.charAt(s.length-1)=='\n')
s=s.substring(0,s.length-1);
funcText.push(s);
funcText.push('");');}
var lines=text.substring(nlPrefix,nlSuffix+1).split('\n');
for(var i=0;i<lines.length;i++){
emitSectionTextLine(lines[i],funcText);
if(i<lines.length-1)
funcText.push('_OUT.write("\\n");\n');}
if(nlSuffix+1<text.length){
funcText.push('if (_FLAGS.keepWhitespace == true) _OUT.write("');
var s=text.substring(nlSuffix+1).replace('\n','\\n');
if(s.charAt(s.length-1)=='\n')
s=s.substring(0,s.length-1);
funcText.push(s);
funcText.push('");');}}
var emitSectionTextLine=function(line,funcText){
var endMarkPrev='}';
var endExprPrev=-1;
while(endExprPrev+endMarkPrev.length<line.length){
var begMark="${",endMark="}";
var begExpr=line.indexOf(begMark,endExprPrev+endMarkPrev.length);
if(begExpr<0)
break;
if(line.charAt(begExpr+2)=='%'){
begMark="${%";
endMark="%}";}
var endExpr=line.indexOf(endMark,begExpr+begMark.length);
if(endExpr<0)
break;
emitText(line.substring(endExprPrev+endMarkPrev.length,begExpr),funcText);
var exprArr=line.substring(begExpr+begMark.length,endExpr).replace(/\|\|/g,"#@@#").split('|');
for(var k in exprArr){
if(exprArr[k].replace)
exprArr[k]=exprArr[k].replace(/#@@#/g,'||');}
funcText.push('_OUT.write(');
emitExpression(exprArr,exprArr.length-1,funcText);
funcText.push(');');
endExprPrev=endExpr;
endMarkPrev=endMark;}
emitText(line.substring(endExprPrev+endMarkPrev.length),funcText);}
var emitText=function(text,funcText){
if(text==null||
text.length<=0)
return;
text=text.replace(/\\/g,'\\\\');
text=text.replace(/\n/g,'\\n');
text=text.replace(/"/g,'\\"');
funcText.push('_OUT.write("');
funcText.push(text);
funcText.push('");');}
var emitExpression=function(exprArr,index,funcText){
var expr=exprArr[index];
if(index<=0){
funcText.push(expr);
return;}
var parts=expr.split(':');
funcText.push('_MODIFIERS["');
funcText.push(parts[0]);
funcText.push('"](');
emitExpression(exprArr,index-1,funcText);
if(parts.length>1){
funcText.push(',');
funcText.push(parts[1]);}
funcText.push(')');}
var cleanWhiteSpace=function(result){
result=result.replace(/\t/g,"    ");
result=result.replace(/\r\n/g,"\n");
result=result.replace(/\r/g,"\n");
result=result.replace(/^(\s*\S*(\s+\S+)*)\s*$/,'$1');
return result;}
var scrubWhiteSpace=function(result){
result=result.replace(/^\s+/g,"");
result=result.replace(/\s+$/g,"");
result=result.replace(/\s+/g," ");
result=result.replace(/^(\s*\S*(\s+\S+)*)\s*$/,'$1');
return result;}
TrimPath.parseDOMTemplate=function(elementId,optDocument,optEtc){
if(optDocument==null)
optDocument=document;
var element=optDocument.getElementById(elementId);
var content=element.value;
if(content==null)
content=element.innerHTML;
content=content.replace(/&lt;/g,"<").replace(/&gt;/g,">");
return TrimPath.parseTemplate(content,elementId,optEtc);}
TrimPath.processDOMTemplate=function(elementId,context,optFlags,optDocument,optEtc){
return TrimPath.parseDOMTemplate(elementId,optDocument,optEtc).process(context,optFlags);}})();
/*pagination*/
jQuery.fn.pagination=function(maxentries,opts){
opts=jQuery.extend({
items_per_page:10,
num_display_entries:10,
current_page:0,
num_edge_entries:0,
link_to:"#",
prev_text:"Prev",
next_text:"Next",
ellipse_text:"...",
prev_show_always:true,
next_show_always:true,
callback:function(){return false;}},opts||{});
return this.each(function(){
function numPages(){
return Math.ceil(maxentries/opts.items_per_page);}
function getInterval(){
var ne_half=Math.ceil(opts.num_display_entries/2);
var np=numPages();
var upper_limit=np-opts.num_display_entries;
var start=current_page>ne_half?Math.max(Math.min(current_page-ne_half,upper_limit),0):0;
var end=current_page>ne_half?Math.min(current_page+ne_half,np):Math.min(opts.num_display_entries,np);
return[start,end];}
function pageSelected(page_id,evt){
current_page=page_id;
drawLinks();
var continuePropagation=opts.callback(page_id,panel);
if(!continuePropagation){
if(evt.stopPropagation){
evt.stopPropagation();}
else{
evt.cancelBubble=true;}}
return continuePropagation;}
function drawLinks(){
panel.empty();
var interval=getInterval();
var np=numPages();
if(np==1){
$(".Pagination").css({display:"none"});}
var getClickHandler=function(page_id){
return function(evt){return pageSelected(page_id,evt);}}
var appendItem=function(page_id,appendopts){
page_id=page_id<0?0:(page_id<np?page_id:np-1);
appendopts=jQuery.extend({text:page_id+1,classes:""},appendopts||{});
if(page_id==current_page){
var lnk=$("<a href='javascript:void(0)' class='current'>"+(appendopts.text)+"</a>");}
else{
var lnk=$("<a>"+(appendopts.text)+"</a>")
.bind("click",getClickHandler(page_id))
.attr('href',opts.link_to.replace(/__id__/,page_id));}
if(appendopts.classes){lnk.addClass(appendopts.classes);}
panel.append(lnk);}
if(opts.prev_text&&(current_page>0||opts.prev_show_always)){
appendItem(current_page-1,{text:opts.prev_text,classes:"prev"});}
if(interval[0]>0&&opts.num_edge_entries>0){
var end=Math.min(opts.num_edge_entries,interval[0]);
for(var i=0;i<end;i++){
appendItem(i);}
if(opts.num_edge_entries<interval[0]&&opts.ellipse_text){
jQuery("<span>"+opts.ellipse_text+"</span>").appendTo(panel);}}
for(var i=interval[0];i<interval[1];i++){
appendItem(i);}
if(interval[1]<np&&opts.num_edge_entries>0){
if(np-opts.num_edge_entries>interval[1]&&opts.ellipse_text){
jQuery("<span>"+opts.ellipse_text+"</span>").appendTo(panel);}
var begin=Math.max(np-opts.num_edge_entries,interval[1]);
for(var i=begin;i<np;i++){
appendItem(i);}}
if(opts.next_text&&(current_page<np-1||opts.next_show_always)){
appendItem(current_page+1,{text:opts.next_text,classes:"next"});}}
var current_page=opts.current_page;
maxentries=(!maxentries||maxentries<0)?1:maxentries;
opts.items_per_page=(!opts.items_per_page||opts.items_per_page<0)?1:opts.items_per_page;
var panel=jQuery(this);
this.selectPage=function(page_id){pageSelected(page_id);}
this.prevPage=function(){
if(current_page>0){
pageSelected(current_page-1);
return true;}
else{
return false;}}
this.nextPage=function(){
if(current_page<numPages()-1){
pageSelected(current_page+1);
return true;}
else{
return false;}}
drawLinks();});};
/*showTip2*/
var display2=false;
function showTip2(proobj){
	var TipDivW=$(proobj).width();
	var TipDivH=$(proobj).height();
	var TipDiv=$("<div id='c02tip' style='z-index:20000;position:absolute;width:"+eval(TipDivW+5)+"px;height:"+eval(TipDivH+5)+"px;'><div style='position:absolute;margin:5px 0 0 5px;width:"+TipDivW+"px;height:"+TipDivH+"px;background:#BCBEC0;z-index:20001;'></div></div>");
	if(display2==false){
		TipDiv.append($(proobj));
		$(document.body).prepend(TipDiv);
		$(proobj).show();
		display2=true;
	}else{
		$("#c02tip").show();
	}
	$("#c02tip").css({top:parseInt(document.documentElement.scrollTop+(document.documentElement.clientHeight-$("#c02tip").height())/2 )+"px",left:(document.documentElement.clientWidth-$("#c02tip").width())/2+"px"})
	$(".Tip_Close").click(function(){
		$("#c02tip").fadeOut();
	});
	timer_5=setTimeout("showTime()",1000);
}
function showTime(){
	clearTimeout(timer_5);
	var overtime=parseInt($("#timer_5").html())-1;
	if(overtime==0){
		$("#c02tip").fadeOut();
		return;
	}
	$("#timer_5").html(overtime);
	timer_5=setTimeout("showTime()",1000)
}
/*coll*/
var homeServiceUrl = "http://jd2008.360buy.com/homeservice.aspx?callback=?";
$(".btn-coll").livequery("click",function() {
	var current = $(this);
	$.login({
		complete: function(result) {
			if (result != null && result.IsAuthenticated != null && result.IsAuthenticated) {
				var productId = parseInt(current.attr("id").replace("coll", ""));
				if (productId > 0) {
					$.getJSON(homeServiceUrl, {
						method: "SaveFavorite",
						productId: productId
					},
					function(result) {
						if (result.SaveFavorite != null) {
							/*$.jdThickBox({
								type:"text",
								source:result.SaveFavorite.process(result),
								width:300,
								height:200,
								_titleOn:false
							})*/
							//alert(result.SaveFavorite.process(result))
                            $("#Collect_Tip").html(result.SaveFavorite.process(result));
                            showTip2("#Collect_Tip");
						}
					});
				}
			}
		}
	});
});
/*mycart*/
var miniCartServiceUrl = "http://jd2008.360buy.com/purchase/minicartservice.aspx?tmp=0&callback=?";
refreshMiniCart();
$(function(){
	var object=$("#o-mycart-list");
	$("#i-mycart").bind("mouseover",function(){
		object.fadeIn();
	}).bind("mouseleave",function(){
		object.fadeOut();
	})

})
function refreshMiniCart() {
	$.login({
		automatic: false,
		complete: function(result) {
			$.getJSON(miniCartServiceUrl, {
				method: "GetCart"
			},
			function(result) {
				if (result.Cart != null && result.GetCartNew != null) {
					$("#mycart-amount").text(result.Cart.Num);
					$("#mycart-list").html(result.GetCartNew.process(result))
				}
			})
		}
	})
}
$("#mycart-list .extra a[id][name]").livequery("click",function() {
	var cartId = parseInt($(this).attr("id"));
	var cartType = $(this).attr("name");
	if (cartId > 0 && cartType != "") {
		$.getJSON(miniCartServiceUrl, {
			method: cartType,
			cartId: cartId
		},
		function(result) {
			if (result.Result) {
				refreshMiniCart()
			}
		})
	}
});
$("#btn-savetocart").livequery("click",function() {
	$.login({
		complete: function(result) {
			if (result.IsAuthenticated) {
				$.getJSON(miniCartServiceUrl, {
					method: "SaveCart"
				},
				function(result) {
					if (result.Result) {
						alert("寄存购物车成功")
					}
				})
			}
		}
	})
});

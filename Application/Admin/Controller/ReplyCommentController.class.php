<?php
namespace Admin\Controller;
/**
 * 后台回复商品评论控制器
 *
 */
class ReplyCommentController extends AdminController {
    /**
     * 回复商品评论管理列表
     *
     */
    public function index(){
     	$list = $this->lists('reply_comment','' ,'id desc');
        $this->assign('list', $list);
		$this->meta_title = '回复商品评论管理';
        $this->display();
    }

    /**
     * 编辑回复商品评论
     */
    public function edit($id = null, $pid = 0){
        $reply = D('reply_comment');

        if(IS_POST){ //提交表单
            if(false !== $reply->update()){
                $this->success('编辑成功！', U('index'));
            } else {
                $error = $reply->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }
        } else {
            $cate = '';
            if($pid){
                /* 获取上级回复信息 */
                $cate = $reply->info($pid, 'id,name,title,status');
                if(!($cate && 1 == $cate['status'])){
                    $this->error('指定的上级回复不存在或被禁用！');
                }
            }

            /* 获取回复信息 */
            $id = I('get.id');
            $info = $id ? $reply->info($id) : '';

            $this->assign('info',       $info);
            $this->assign('reply',   $cate);
            $this->meta_title = '编辑回复商品评论';
            $this->display();
        }
    }

    /**
     *新增回复商品评论
     */
    public function add($pid = 0){
        $reply = D('reply_comment');

        if(IS_POST){ //提交表单
            if(false !== $reply->update()){
                $this->success('新增成功！', U('index'));
            } else {
                $error = $reply->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }
        } else {
            $cate = array();
            if($pid){
                /* 获取上级回复信息 */
                $cate = $reply->info($pid, 'id,name,title,status');
                if(!($cate && 1 == $cate['status'])){
                    $this->error('指定的上级回复不存在或被禁用！');
                }
            }

            /* 获取回复信息 */
            $this->assign('info',       null);
            $this->assign('reply', $cate);
            $this->meta_title = '新增回复商品评论';
            $this->display('edit');
        }
    }

    /**
     * 删除一个回复商品评论
     */
	public function del(){
	    if(IS_GET){
			$id=I('get.id');
	        $reply =   M('reply_comment');
	        if( $reply->where("id='$id'")->delete()){
	            $this->success('删除成功');
	        }else{
			 	$this->error('删除失败');
	        }
		}

        if(IS_POST){
            $ids = I('post.id');
            $reply = M("reply_comment");
            if(is_array($ids)){
                foreach($ids as $id){
                    $reply->where("id='$id'")->delete();
                }
            }
           $this->success("删除成功！");
        }
	}

}

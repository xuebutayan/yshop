<?php


namespace Admin\Controller;
use Think\Model;
/**
 * 后台商品类型控制器
  *
 */
class GoodsAttributeController extends AdminController {
    public function index(){
      $cat_id = I('request.goods_type');
      empty($cat_id) && $this->error('未指定商品类型！');
      $count = M()->table('__GOODS_ATTRIBUTE__ a')->field('a.*,t.cat_name')->join('__GOODS_TYPE__ t on a.cat_id=t.cat_id','LEFT')->where(' a.cat_id='.$cat_id)->order('sort_order asc')->count();
      //分页
      $listRows = C('LIST_ROWS') > 0 ? C('LIST_ROWS') : 10;
      $page = new \Think\Page($count,$listRows);
      $p =$page->show();
      //列表
      $list = M()->table('__GOODS_ATTRIBUTE__ a')->field('a.*,t.cat_name')->join('__GOODS_TYPE__ t on a.cat_id=t.cat_id','LEFT')->where(' a.cat_id='.$cat_id)->order('sort_order asc')->limit($page->firstRow.','.$page->listRows)->select();

      // 记录当前列表页的cookie
      Cookie('__forward__',$_SERVER['REQUEST_URI']);
      $this->assign('_list', $list);
      $this->assign('_page', $p? $p: '');
      $this->meta_title = '商品属性';
      $this->display();
    }
    function add(){
      if(IS_POST){
        $goods_attribute = M('GoodsAttribute');
        $rules = array(
          array('attr_name','require','属性名称不能为空！',Model::MUST_VALIDATE,'unique',Model::MODEL_BOTH),
          array('cat_id','require','商品类型必须选择！',Model::MUST_VALIDATE),
        );
        if(false===$goods_attribute->validate($rules)->create()) $this->error($goods_attribute->getError());
        else{
          if(false!==$goods_attribute->add()) $this->success('新增商品属性成功！');
          else $this->error('新增商品属性失败！');
        }
      }else{
        $goods_types = M('GoodsType')->field('cat_id,cat_name')->where(array('enabled'=>1))->select();
        $cat_id = intval($_GET['goods_type']);
        $goods_type_html = '<option value=0>请选择...</option>';
        foreach ($goods_types as $v) {
            $selected = $cat_id==$v['cat_id']?'selected':'';
            $goods_type_html.='<option value="'.$v['cat_id'].'" '.$selected.'>'.$v['cat_name'].'</option>';
        }
        $this->assign('goods_type_html',$goods_type_html);
        $this->meta_title = '新增商品属性';
        $this->display('edit');
      }
    }
    function edit(){
      if(IS_POST){
        $goods_attribute = M('GoodsAttribute');
        $rules = array(
          array('attr_name','require','属性名称不能为空！',Model::MUST_VALIDATE,'unique',Model::MODEL_BOTH),
          array('cat_id','require','商品类型必须选择！',Model::MUST_VALIDATE),
        );
        if(false===$goods_attribute->validate($rules)->create()) $this->error($goods_attribute->getError());
        else{
          if(false!==$goods_attribute->save()) $this->success('编辑商品属性成功！');
          else $this->error('编辑商品属性失败！');
        }
      }else{
        $attr_id = I('request.attr_id');
        empty($attr_id) && $this->error('未指定商品属性！');
        $info = M('GoodsAttribute')->find($attr_id);
        //商品类型
        $goods_types = M('GoodsType')->field('cat_id,cat_name')->where(array('enabled'=>1))->select();
        $goods_type_html = '';
        foreach ($goods_types as $v) {
            $selected = $info['cat_id']==$v['cat_id']?'selected':'';
            $goods_type_html.='<option value="'.$v['cat_id'].'" '.$selected.'>'.$v['cat_name'].'</option>';
        }
        $this->assign('goods_type_html',$goods_type_html);
        $this->assign('info',$info);
        $this->meta_title = '编辑商品属性';
        $this->display('edit');
      }
    }
    function delete(){
      $id = I('request.id');
      if(M('GoodsType')->delete($id)){
          $this->success('删除成功');
      } else {
          $this->error('删除失败！');
      }
    }
}
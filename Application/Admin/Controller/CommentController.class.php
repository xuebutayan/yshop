<?php
namespace Admin\Controller;
/**
 * 后台评论管理控制器
 */
class CommentController extends AdminController{
	/**
	 * 商品评论列表
	 */
	public function index(){
		$list = $this->lists('Comment',"" ,'id desc');
		$this->assign('list', $list);
		$this->meta_title = '商品评论管理';
		$this->display();
	}

	/**
	 * 新增商品评论
	 */
	public function add(){
		$comment = D('Comment');
        if(IS_POST){ //提交表单
            if(false !== $comment->update()){
                $this->success('新增成功！', U('index'));
            } else {
                $error = $Message->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }

        } else {
            $cate = '';
            if($pid){
                /* 获取上级评论信息 */
                $cate = $comment->info($pid, 'id,name,title,status');
                if(!($cate && 1 == $cate['status'])){
                    $this->error('指定的上级商品评论不存在或被禁用！');
                }
            }
            /* 获取评论信息 */
            $info = $id ? $comment->info($id) : '';
            $this->assign('info',       $info);
            $this->assign('comment',   $cate);
            $this->meta_title = '编辑商品评论';
            $this->display('edit');
        }
	}

	/**
	 * 修改商品评论
	 */
	public function edit(){
		$comment = D('Comment');
        if(IS_POST){ //提交表单
            if(false !== $comment->update()){
                $this->success('编辑成功！', U('index'));
            } else {
                $error = $comment->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }
        } else {
            $cate = '';
            if($pid){
                /* 获取上级评论信息 */
                $cate = $comment->info($pid, 'id,name,title,status');
                if(!($cate && 1 == $cate['status'])){
                    $this->error('指定的上级商品评论不存在或被禁用！');
                }
            }
            $id = I('get.id');
            /* 获取评论信息 */
            $info = $id ? $comment->info($id) : '';

            $this->assign('info',      $info);
            $this->assign('comment',   $cate);
            $this->meta_title = '编辑商品评论';
            $this->display();
        }
	}

	/**
	 * 删除商品评论
	 */
	public function del(){
        if(IS_GET){
  		    $id=I('get.id');
            $document   =   M('Comment');
            if( $document->where("id='$id'")->delete()){
                $this->success('删除成功');
            }else{
    		        $this->error('删除失败');
            }
  		  }

        if(IS_POST){
            $ids = I('post.id');
            $document = M("Comment");
            if(is_array($ids)){
                foreach($ids as $id){
                    $document->where("id='$id'")->delete();
                }
            }
            $this->success("删除成功！");
        }
	}

	/**
	 * 回复商品评论
	 */
	 public function reply($id = null, $pid = 0){
        if(IS_POST){ //提交表单
			 $reply = D('reply_comment');

            if(false !==  $reply ->update()){
                $this->success('回复成功！', U('index'));
            } else {
                $error =  $reply ->getError();
                $this->error(empty($error) ? '未知错误！' : $error);
            }
        } else {
            $cate = '';
            if($pid){
                /* 获取上级商品评论信息 */
                $cate =  $reply ->info($pid, 'id,name,title,status');
                if(!($cate && 1 == $cate['status'])){
                    $this->error('指定的上级留言不存在或被禁用！');
                }
            }
            $id = I('get.id');
            $this->assign('commentid',$id);

            // $Comment = D('reply_comment');
            // $info = $id ? $Comment->info($id) : '';
            // $this->assign('info',$info);
            $this->meta_title = '回复商品评论';
            $this->display();
        }
    }

}
?>
<?php


namespace Admin\Controller;

/**
 * 后台商品类型控制器
  *
 */
class GoodsTypeController extends AdminController {
    public function index(){
     // $sql = 'SELECT t.*, COUNT(a.cat_id) AS attr_count FROM '.C('DB_PREFIX').'goods_type` AS t LEFT JOIN '.C('DB_PREFIX').'attribute` AS a ON a.cat_id=t.cat_id GROUP BY t.cat_id';
      $count = M()->table(C('DB_PREFIX').'goods_type t')->field('t.*,COUNT(a.cat_id) AS attr_count')->join(C('DB_PREFIX').'goods_attribute a on a.cat_id=t.cat_id','LEFT')->group('t.cat_id')->count();
      //分页
      $listRows = C('LIST_ROWS') > 0 ? C('LIST_ROWS') : 10;
      $page = new \Think\Page($count,$listRows);
      $p =$page->show();
      //列表
      $list = M()->table(C('DB_PREFIX').'goods_type t')->field('t.*,COUNT(a.cat_id) AS attr_count')->join(C('DB_PREFIX').'goods_attribute a on a.cat_id=t.cat_id','LEFT')->group('t.cat_id')->limit($page->firstRow.','.$page->listRows)->select();

      // 记录当前列表页的cookie
      Cookie('__forward__',$_SERVER['REQUEST_URI']);

      $this->assign('_list', $list);
      $this->assign('_page', $p? $p: '');
      $this->meta_title = '商品类型';
      $this->display();
    }
    function add(){
      if(IS_POST){
        $goods_type = D('GoodsType');
        if(false===$goods_type->create()) $this->error($goods_type->getError());
        else{
          if(false!==$goods_type->add()) $this->success('新增商品类型成功！');
          else $this->error('新增商品类型失败！');
        }
      }else{
        $this->meta_title = '新增商品类型';
        $this->display('edit');
      }
    }
    function edit(){
      if(IS_POST){
        $goods_type = D('GoodsType');
        if(false===$goods_type->create()) $this->error($goods_type->getError());
        else{
          if(false!==$goods_type->save()) $this->success('编辑商品类型成功！');
          else $this->error('编辑商品类型失败！');
        }
      }else{
        $this->meta_title = '编辑商品类型';
        $this->display('edit');
      }
    }
    function delete(){
      $id = I('request.id');
      if(M('GoodsType')->delete($id)){
          $this->success('删除成功');
      } else {
          $this->error('删除失败！');
      }
    }
}
<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com>
// +----------------------------------------------------------------------

namespace Admin\Model;
use Think\Model;

/**
 * 商品类型模型
 */

class GoodsTypeModel extends Model {
    protected $_validate = array(
        array('cat_name', 'require', '商品类型名称不能为空', self::MUST_VALIDATE , 'unique', self::MODEL_BOTH),
    );

}

<?php
class Page3{
    protected  $total_rows;    // 总条数
    protected  $list_rows;    // 每页显示条数
    protected  $sub_pages;   // 预定显示页数
    protected  $page_name;        //分页名称
    protected  $url;              //每个分页的链接    exp: 'register_all.php?p='
    protected  $now_page;    // 当前页

    protected  $first_row;    // 起始条数
    protected  $total_pages;     // 总页数
    protected  $page_array=array();//用来构造分页的数组

     /**
      * 作用：构造函数
      * 参数： array   一维
      */
    public function __construct($data=array('total_row'=>'','list_rows'=>'','sub_pages'=>'','page_name'=>'','url'=>'','now_page'=>'')){
        $this ->total_rows = $data['total_rows'];
        $this ->list_rows = $data['list_rows'];
        $this ->sub_pages = $data['sub_pages'];
        $this ->page_name = !empty($data['page_name'])?$data['page_name']:'p';        //分页名称
        $this ->url = $data['url'];


        $this->total_pages = ceil($this->total_rows/$this->list_rows);  // 总页数

            //当前页
        if(!empty($data['now_page'])){
            $this->now_page = intval($data['now_page']);
        }else{
            $this->now_page = !empty($this->page_name) ? $_GET['p']:1;
        }
        $this->now_page = $this->now_page <=0 ? 1:$this->now_page;
        if(!empty($this->total_pages) && $this->now_page > $this->total_pages){
            $this->now_page = $this->total_pages;
        }

            // 起始条数
        $this->first_row = $this->list_rows * ($this->now_page - 1);
    }


    /**
     *  作用：用来给建立分页的数组初始化的函数
     *  参数组 : void
     *  retrun : array 预定显示的页码
     */
    protected function initArray(){
        for($i=1; $i<= $this->sub_pages; $i++){
            $this->page_array[$i] = $i;
        }
        return $this->page_array;
    }


    /**
     * 作用：构造显示的条数(寄｜偶) exp: 1 2 3 4 5 6 7 8 9 10  || 1 2 3 4 5
     * 参数：void
     * return: array 由当前显示的页数组成的数组
     */
    public function contsruct_num_Page(){
    	// 预定显示页数为寄数时
	    	// 如果总页数小于等于预定显示页数加二，则全部显示   如： 1 2 3 4 5 6 7
	    	// 如果总页数大于等于预定显示页数加二
	    	    // 如果当前页小于等于预定显示页数加一的一半并加一   如：  1 2 3 4 5 6 <a> ... </a>. 尾页 下一页
	    	    // 如是当前页大于总页数减去预定显示页数加一的一半   如： 上一页 1<a> ... </a>   385 386 387 389
	    	    // 其它     如： 上 一 页 1 <a> ... </a> 23 24 25 26 27 <a> ... </a> 389 下一页

	    //	预定显示页数为偶数时
	        // 如果总页数小于等于预定显示页数加二，则全部显示    如： 1 2 3 4 5 6 7 8
	        // 如果总页数大于等于预定显示页数加二
	            //  如果当前页小于等于预定显示页数的一半并加二   如：上一页 1 2 3 4 5 6 7 <a> ... </a>尾页 下一页
	            //  如是当前页大于总页数减去预定显示页的一半    如： 上一页 1 <a> ... </a>  385 386 387 389 下一页
	            //   // 其它     如： 上 一 页 1 <a> ... </a> 23 24 25 26 27 28 <a> ... </a> 389 下一页
	    $subPageCssStr = '';

	    if($this->sub_pages%2 == 1){  // 预定显示页数为寄数时
	    	// 如果总页数小于等于预定显示页数加二，则全部显示   如： 1 2 3 4 5 6 7
	    	if($this->total_pages <= $this->sub_pages +2){
	    		$current_array = array();
                for($i=1; $i <= $this->total_pages;$i++){
                    $current_array[$i] = $i;
                }

                foreach($current_array as $val){
		            if($val == $this->now_page){
		                $subPageCssStr .= '<a class="on_min_page" onclick="page_click(this);return false;" rel="'.$this->now_page.'" href="javascript:void(0);">'.$this->now_page.'</a>';
                    }else{
		                $subPageCssStr .=  '<a onclick="page_click(this);return false;" rel="'.$val.'" href="javascript:void(0);">'.$val.'</a>';
		            }
		        }

            // 如果总页数大于等于预定显示页数加二
	    	}else{
	    		// 如果当前页小于等于预定显示页数加一的一半并加一   如：  1 2 3 4 5 6 <a> ... </a>. 尾页 下一页
	    		if($this->now_page <= ($this->sub_pages+1)/2+1){
                    $current_array = $this->initArray(); // 1 2 3 4 5
                    foreach($current_array as $key=>$val){
                        $current_array[$key+1] = $val + 1;  // 1 2 3 4 5 6
                    }

                    foreach($current_array as $val){
                        if($val == $this->now_page){
                            $subPageCssStr .= '<a class="on_min_page" onclick="page_click(this);return false;" rel="'.$this->now_page.'" href="javascript:void(0);">'.$this->now_page.'</a>';
                        }else{
                            $subPageCssStr .=  '<a onclick="page_click(this);return false;" rel="'.$val.'" href="javascript:void(0);">'.$val.'</a>';
                        }
                    }
			        $subPageCssStr .= '<a> ... </a> <a class="min_page_biao" onclick="page_click(this);return false;" rel="'.$this->total_pages.'" href="javascript:void(0);">'.$this->total_pages.'</a>';
                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;"  rel="'.($this->now_page + 1).'" href="javascript:void(0);">下一页</a>';

                // 如是当前页大于总页数减去预定显示页数加一的一半   如： 上一页 1<a> ... </a>   385 386 387 389
	    		}else if($this->now_page >= $this->total_pages - ($this->sub_pages+1)/2){
	    			$current_array = array();
                    for($i=$this->total_pages;($this->total_pages - $this->sub_pages) < $i;$i--){
                        $current_array[$i-1] = $i;
                    }
                    foreach($current_array as $key=>$val){
						$current_array[$key-1] = $val-1;
					}
                    $current_array=array_reverse($current_array);  // 倒序

                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;" rel="'.($this->page + 1).'" href="javascript:void(0);">上一页</a>';
                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;" rel="1" href="javascript:void(0);">1</a> <a> ... </a> ';
                    foreach($current_array as $val){
			            if($val == $this->now_page){
			                $subPageCssStr .= '<a class="on_min_page" onclick="page_click(this);return false;" rel="'.$this->now_page.'" href="javascript:void(0);">'.$this->now_page.'</a>';
			            }else{
			                $subPageCssStr .=  '<a onclick="page_click(this);return false;" rel="'.$val.'" href="javascript:void(0);">'.$val.'</a>';
			            }
			        }

                // 其它     如： 上 一 页 1 <a> ... </a> 23 24 25 26 27 <a> ... </a> 389 下一页
	    		}else{
	    			$current_array = array();
                    $a = $this->now_page - ($this->sub_pages-1)/2;
                    $b = $this->now_page + ($this->sub_pages-1)/2;
                    for($i = $a; $i <= $b; $i++){
                        $current_array[$i-1] = $i;
                    }

                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;" rel="'.($this->now_page + 1).'" href="javascript:void(0);">上一页</a>';
                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;" rel="1" href="javascript:void(0);">1</a> <a> ... </a> ';
                    foreach($current_array as $val){
			            if($val == $this->now_page){
			                $subPageCssStr .= '<a class="on_min_page" onclick="page_click(this);return false;" rel="'.$this->now_page.'" href="javascript:void(0);">'.$this->now_page.'</a>';
			            }else{
			                $subPageCssStr .=  '<a onclick="page_click(this);return false;" rel="'.$val.'" href="javascript:void(0);">'.$val.'</a>';
			            }
			        }
                    $subPageCssStr .= ' <a> ... </a> <a class="min_page_biao" onclick="page_click(this);return false;" rel="'.$this->total_pages.'" href="javascript:void(0);">'.$this->total_pages.'</a>';
                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;"  rel="'.($this->now_page - 1).'" href="javascript:void(0);">下一页</a>';

	    		}
	    	}
	    	return $subPageCssStr;

	    }else if($this->sub_pages%2 == 0){  //	预定显示页数为偶数时
	    	// 如果总页数小于等于预定显示页数加二，则全部显示 如： 1 2 3 4 5 6 7 8
	    	if($this->total_pages <= $this->sub_pages +2){
	    		$current_array = array();
                for($i=1; $i <= $this->total_pages;$i++){
                    $current_array[$i] = $i;
                }

                foreach($current_array as $val){
		            if($val == $this->now_page){
		                $subPageCssStr .= '<a class="on_min_page" onclick="page_click(this);return false;" rel="'.$this->now_page.'" href="javascript:void(0);">'.$this->now_page.'</a>';
		            }else{
		                $subPageCssStr .=  '<a onclick="page_click(this);return false;" rel="'.$val.'" href="javascript:void(0);">'.$val.'</a>';
		            }
		        }

            // 如果总页数大于等于预定显示页数加二
	    	}else{
	    		// 如果当前页小于等于预定显示页数的一半并加二   如：上一页 1 2 3 4 5 6 7 <a> ... </a>尾页 下一页
	    		if($this->now_page <= ($this->sub_pages)/2+2){
	    			$current_array = $this->initArray(); // 1 2 3 4 5
                    foreach($current_array as $key=>$val){
                        $current_array[$key+1] = $val + 1;  // 1 2 3 4 5 6
                    }

                    foreach($current_array as $val){
			            if($val == $this->now_page){
			                $subPageCssStr .= '<a class="on_min_page" onclick="page_click(this);return false;" rel="'.$this->now_page.'" href="javascript:void(0);">'.$this->now_page.'</a>';
			            }else{
			                $subPageCssStr .=  '<a onclick="page_click(this);return false;" rel="'.$val.'" href="javascript:void(0);">'.$val.'</a>';
			            }
			        }
			        $subPageCssStr .= ' <a> ... </a> <a class="min_page_biao" onclick="page_click(this);return false;" rel="'.$this->total_pages.'" href="javascript:void(0);">'.$this->total_pages.'</a>';
                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;"  rel="'.($this->now_page + 1).'" href="javascript:void(0);">下一页</a>';

                //  如是当前页大于总页数减去预定显示页的一半    如： 上一页 1 <a> ... </a>  385 386 387 389 下一页
	    		}else if($this->now_page >= $this->total_pages - ($this->sub_pages)/2){
	    			$current_array = array();
                    for($i=$this->total_pages;($this->total_pages - $this->sub_pages) < $i;$i--){
                        $current_array[$i-1] = $i;
                    }
                    foreach($current_array as $key=>$val){
						$current_array[$key-1] = $val-1;
					}
                    $current_array=array_reverse($current_array);  // 倒序

                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;" rel="'.($this->now_page + 1).'" href="javascript:void(0);">上一页</a>';
                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;" rel="1" href="javascript:void(0);">1</a> <a> ... </a> ';
                    foreach($current_array as $val){
			            if($val == $this->now_page){
			                $subPageCssStr .= '<a class="on_min_page" onclick="page_click(this);return false;" rel="'.$this->now_page.'" href="javascript:void(0);">'.$this->now_page.'</a>';
			            }else{
			                $subPageCssStr .=  '<a onclick="page_click(this);return false;" rel="'.$val.'" href="javascript:void(0);">'.$val.'</a>';
			            }
			        }

			    // 其它     如： 上 一 页 1 <a> ... </a> 23 24 25 26 27 28 <a> ... </a> 389 下一页
	    		}else{
	    			$current_array = array();
                    $a = $this->now_page - $this->sub_pages/2;
                    $b = $this->now_page + $this->sub_pages/2 - 1;
                    for($i = $a; $i <= $b; $i++){
                        $current_array[$i-1] = $i;
                    }

                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;" rel="'.($this->now_page + 1).'" href="javascript:void(0);">上一页</a>';
                    $subPageCssStr .= '<a class="min_page_biao" onclick="page_click(this);return false;" rel="1" href="javascript:void(0);">1</a> <a> ... </a> ';
                    foreach($current_array as $val){
			            if($val == $this->now_page){
			                $subPageCssStr .= '<a class="on_min_page" onclick="page_click(this);return false;" rel="'.$this->now_page.'" href="javascript:void(0);">'.$this->now_page.'</a>';
			            }else{
			                $subPageCssStr .=  '<a onclick="page_click(this);return false;" rel="'.$val.'" href="javascript:void(0);">'.$val.'</a>';
			            }
			        }
                    $subPageCssStr .= ' <a> ... </a> <a class="min_page_biao" onclick="page_click(this);return false;" rel="'.$this->total_pages.'" href="javascript:void(0);">'.$this->total_pages.'</a>';
	    		}
	        }
	    	return $subPageCssStr;
	    }

    }

}
?>
<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------
namespace Home\Controller;
/**
 * 换货模型控制器
 * 文档模型列表和详情
 */
class ExchangeController extends HomeController {
    /* 换货申请 */
    public function index(){
        if ( !is_login() ) {
		    $this->error( "您还没有登陆",U("User/login") );
		}
		$uid = is_login();

		if (IS_POST){
		    $id= I('post.id',0,'intval');//获取id
            if(!is_numeric($id)){
		        $this->error('ID错误！');
		    }
            $info=M("shoplist")->where(array('id'=>$id,'uid'=>$uid))->find();

			$postnum=I('post.num');
            if(!is_numeric($postnum)){
		         $this->error('数量错误！');
		    }
            if($postnum<1 && $postnum>$info['num']){
		        $this->error('无效数量！');
		    }

		    $pics = I('post.pics');
		    if(!empty($pics)){
		    	$pics = substr($pics,1);
		    }

			$num= $info['num'];//获取num
			$price=$info['price'];//获取价格
			$goodid=$info['goodid'];//获取goodid
			$parameters=$info['parameters'];
			if ($postnum>$num) {
				 $this->error('超出购买数量');
			} else {
			 	// 保存信息到换货表
			 	$Exchange=D("Exchange");
				$Exchange->create();
				$Exchange->create_time=NOW_TIME;
				$Exchange->total=$num*$price;
				$Exchange->goodid=$goodid;
				$Exchange->status=1;
				$Exchange->parameters=$parameters;
				$back->pics=$pics;
				$Exchange->uid=$uid;

				$existEx = $Exchange->where(array('id'=>$id,'goodid'=>$goodid,'uid'=>$uid))->find();

				if(!empty($existEx)){
					$this->error('该商品已申请过换货',U('Exchange/lists'));  // 已申请过换货
				}

				$Exchange->add();
				addUserLog('申请换货，商品名称'.get_good_name($goodid).'参数'.$parameters, is_login())	;
				$data['status']=-4;			 //更改商品的售后信息
				$shop=M("shoplist");
				if ($shop->where("id='$id'")->save($data)) {
					$this->success('申请成功',U("Exchange/success"));
				} else {
					$this->error('申请失败，或重复操作');
				}
			 }
		}else{
		   $id= I('get.id',0,'intval');//获取id
		   $msg="Tips，提交换货单";
		   $this->meta_title = '填写换货单';
		   $detail=M("shoplist")->find($id);

		   $this->assign('list',$detail);
		   $this->assign('msg',$msg);
		   $this->display('index');
        }
    }

    /* 提交状态 */
	public function success(){
		if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
		$this->display('success');
	}

	/* 换货列表 */
	public function lists(){
		if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
		$map['uid'] = is_login();
		$map['status'] = array('lt',0);

		$shoplist=M("shoplist");
	   	$count = $shoplist->where($map)->count();
	   	$Page= new \Think\Page($count,10);

	   	$list = $shoplist->where($map)->order('id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
	   	foreach($list as $n=> $val){
	    	$where['uid'] = is_login();
	    	$where['tag'] = $val['tag'];
        	$list[$n]['orderId']=M('order')->where($where)->getField('orderid');
        	$list[$n]['cover_id'] = M('document')->where(array('id'=>$val['goodid']))->getField('cover_id');
        	$backWhere['id'] = $val['id'];
        	$backWhere['goodid'] = $val['goodid'];
        	$list[$n]['exchange'] = M('Exchange')->where($backWhere)->find();  // 'num,status,create_time,total,update_time,backname'
		}

		$Page= new \Think\Page($count,10);
		$Page->setConfig('prev','上一页');
	    $Page->setConfig('next','下一页');
	    $Page->setConfig('first','第一页');
      	$Page->setConfig('last','尾页');
        $Page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $showPage = $Page->show();
        $this->assign('list',$list);
        $this->assign('showPage',$showPage);
	   	$this->display('lists');
	}

    /* 换货详情 */
    public function detail(){
        if ( !is_login() ) {
		    $this->error( "您还没有登陆",U("User/login") );
		}
		$id= I('get.id',0,'intval');//获取id
		if(!is_numeric($id)){
		    $this->error('ID错误！');
		}
		$Exchange=M("Exchange");
		$list= $Exchange->where("shopid='$id'")->find();
		$info= M("Exchange")->where("shopid='$id'")->getField("backinfo");
		$this->assign('list',$list);
		$this->assign('info',$info);
		$this->assign('id',$id);
		$this->meta_title = '换货单'.$list['id'].'详情';
		$msg="换货:";
		$this->assign('msg',$msg);
		$this->display('detail');
    }


     /* 买家快递信息 */
    public function exchangekuaidi(){
    	if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}

		$exchange = M("exchange");
		$where['id'] = I('post.exchangeid');
		$data['tool'] = I('post.tool');
		$data['toolid'] = I('post.toolid');
		$data['acceptname'] = I('post.acceptname');
		$data['acceptphone'] = I('post.acceptphone');
		$data['acceptaddress'] = I('post.acceptaddress');
		$re = $exchange->where($where)->save($data);
		if($re){
			$this->success('提交成功',U('Exchange/lists'));
		}else{
			$this->error('提交失败');
		}
    }

}

<?php
// +----------------------------------------------------------------------
// | yershop [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// |  Author: 烟消云散 <1010422715@qq.com>
// +----------------------------------------------------------------------
namespace Home\Controller;
use Think\Controller;
/**
 * 评论模型控制器
 */
class CommentController extends HomeController {
    public function index() {
		if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}

	     $listid=I('get.id',0,'intval');
	     $shoplist=M('shoplist');
	     $list= $shoplist->find($listid);
		//没有评论过
		if ($list["iscomment"]==1){
			  $this->assign('comment', $list);
			  $title=get_good_name($list["goodid"]);
			  $this->meta_title = '评价商品_'.$title;
			  $this->display('index');
	    }else {
		    $this->error('商品评价过');
		}
    }

    /* 添加评论 */
    public function add() {
        if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
        if (IS_POST) {
			 $uid=is_login();
			 $comment = D("comment");
			 $comment->create();
			 $comment->uid = $uid;
			 $comment->create_time = NOW_TIME;
			 $comment->status = 1;
			 $comment->content = I('post.content');

			if(I('post.score') == ''){
				$score = 3;
			}else{
				$score = I('post.score');
			}

			 $comment->score  = $score;
			 $comment->pics = I('post.pics');
			 $comment->anonymity = I('post.niming');
			 $id=$comment->add();
			 if($id>0){
				$this->success('商品评价成功',U('comment/lists'));
				M('shoplist')->where(array('goodid'=>I('post.goodid'),'tag'=>I('post.tag')))->setField("iscomment","2");
			  }
        } else {
	        $this->redirect('商品评价失败');
		}
    }

    /* 评论列表 */
    public  function lists() {
        if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
	    $uid=is_login();
        $map['uid']=$uid;
		$list=D("comment")->getLists($map);
		$this->assign('list',$list);
		$page=D("comment")->getPage($map);
		$this->assign('page',$page);
	    $this->meta_title = get_username().'的评论管理';

	    $bad = M("comment")->where("status='1' and uid='$uid' and score='1'")->count();
	    $common = M("comment")->where("status='1' and uid='$uid' and score='2'")->count();
	    $best = M("comment")->where("status='1' and uid='$uid' and score='3'")->count();
	    $this->assign('bad', $bad);
	    $this->assign('common',$common);
	    $this->assign('best',$best);
	    $this->display('lists');
    }

    /* 查看评论 */
    public function detail(){
    	if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
		$uid = is_login();
		$comment = D('comment')->where(array('id'=>I('get.id'),'uid'=>$uid))->find();
		$comment['pics'] = explode(',',$comment['pics']);

		// 商品描述评价
		$goodscore = intval($comment['goodscore']);
		switch ($goodscore) {
		case 1:
		  $comment['goodScoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		   $comment['goodScoreZN'] = '差';
		  break;
		case 2:
		  $comment['goodScoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		    $comment['goodScoreZN'] = '一般';
		  break;
		case 3:
		  $comment['goodScoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		    $comment['goodScoreZN'] = '较好';
		  break;
		case 4:
		  $comment['goodScoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		    $comment['goodScoreZN'] = '好';
		  break;
		default:
		  $comment['goodScoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-on-big.png" />';
		    $comment['goodScoreZN'] = '非常好';
		}

		// 服务态度评价
		$servicescore = intval($comment['servicescore']);
		switch ($servicescore) {
		case 1:
		  $comment['servicescoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		   $comment['servicescoreZN'] = '差';
		  break;
		case 2:
		  $comment['servicescoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		    $comment['servicescoreZN'] = '一般';
		  break;
		case 3:
		  $comment['servicescoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		    $comment['servicescoreZN'] = '较好';
		  break;
		case 4:
		  $comment['servicescoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		    $comment['servicescoreZN'] = '好';
		  break;
		default:
		  $comment['servicescoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-on-big.png" />';
		    $comment['servicescoreZN'] = '非常好';
		}

		// 物流速度评价
		$deliveryscore = intval($comment['deliveryscore']);
		switch ($deliveryscore) {
		case 1:
		  $comment['deliveryscoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		   $comment['deliveryscoreZN'] = '差';
		  break;
		case 2:
		  $comment['deliveryscoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		    $comment['deliveryscoreZN'] = '一般';
		  break;
		case 3:
		  $comment['deliveryscoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-off-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		    $comment['deliveryscoreZN'] = '较好';
		  break;
		case 4:
		  $comment['deliveryscoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-off-big.png" />';
		    $comment['deliveryscoreZN'] = '好';
		  break;
		default:
		  $comment['deliveryscoreImg'] =  '<img title="差" alt="1" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="一般" alt="2" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="较好" alt="3" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="好" alt="4" src="/public/Home/images/star-on-big.png" />' .
					                  '<img title="非常好" alt="5" src="/public/Home/images/star-on-big.png" />';
		    $comment['deliveryscoreZN'] = '非常好';
		}

		$this->assign('comment',$comment);
		$this->assign('pics',$comment['pics']);
		$this->display('detail');
    }

}

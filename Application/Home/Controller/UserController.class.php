<?php
namespace Home\Controller;
use User\Api\UserApi;

/**
 * 用户控制器
 * 包括用户中心，用户登录及注册
 */
class UserController extends HomeController {

	/* 注册页面 */
	public function register($mobile = "", $password = "", $repassword = "", $email = "", $verify = ""){
		$username =safe_replace($mobile);//过滤
		if(!C("USER_ALLOW_REGISTER")){
            $this->error("注册已关闭");
        }
		if(IS_POST){ //注册用户
			/* 检测密码 */
			if($password != $repassword){
				$this->error("密码和重复密码不一致！");
			}

			/* 检测验证码 */
			if(session('mobileVerify') != I('post.code')){
				$this->error('验证码填写不正确！');
			}

			/* 未接受贴芯点服务协议！ */
			if(I('post.state') != 1){
				$this->error('未接受贴芯点服务协议！');
			}

			/* 调用注册接口注册用户 */
            $User = new UserApi;
			$uid = $User->register(I('post.mobile'), I('post.password'), I('post.email'));   //返回ucentermember数据表用户主键id
			if(0 < $uid){ //注册成功
				//TODO: 发送验证邮件
				 // 配置邮件提醒
				 /*$mail=I('post.email'); //获取会员邮箱
				 $title="注册提醒";
				 $auth=sha1(C('DATA_AUTH_KEY'));
				 $url=$_SERVER['SERVER_NAME'].U("account/confirm_email",array('regid'=>$uid,'type'=>"email",'auth'=>$auth,'url'=>$_SERVER['SERVER_NAME']));
				 $words=sha1($url);
				 $content="您在".C('SITENAME')."注册了账号，<a href=\"".$url."\" target='_blank'>".$words.'</a>请点击激活'.$mail;
				  //记录日志
				 addUserLog('新会员注册', $uid)	;

				if(C('MAIL_PASSWORD')){
					SendMail($mail,$title,$content);  // 未配置邮箱
				}*/

					// 调用登陆
				$this->login(I('post.mobile'), I('post.password'), 3);

			} else { //注册失败，显示错误信息
					$this->error($this->showRegError($uid));
			}
		}else {
	     	$this->meta_title = '会员注册';
			// $this->display('signin/register');
	     	$this->display();
		}
	}


	/* 登录页面 */
	public function login($username = "", $password = "", $verify = ""){
		if(IS_POST){ //登录验证
            $username =safe_replace($username);//过滤

			/* 检测验证码 */
			/*if(!check_verify($verify)){
				$this->error("验证码输入错误！");
			}*/
			/* 调用UC登录接口登录 */
			$uMember = M('ucenter_member');

			if($uMember->where(array('username'=>$username))->find()){
				$type = 1;
			}else if($uMember->where(array('email'=>$username))->find()){
				$type = 2;
			}else if($uMember->where(array('mobile'=>$username))->find()){
				$type = 3;
			}else{
				$type = 4;
			}
			$user = new UserApi;
			$uid = $user->login($username, $password,$type);
            if(0 < $uid){ //UC登录成功
				 if(I('post.remember')){
					   cookie('username',$username,2592000); // 指定cookie保存30天时间
					   cookie('password',$password,2592000); // 指定cookie保存30天时间
					   addUserLog('保存cookie自动登录',$uid);
				 }
				/* 登录用户 */
				$Member = D("Member");
				if($Member->login($uid)){ //登录用户
					//TODO:跳转到登录前页面
					if($_POST['email']){
					   	$msg="注册成功!";
					   	$this->success($msg,U('user/login'));
					}else{
						$arr = session('user_auth');
						if($arr['username'] == ''){
							$re = $uMember->where(array('id'=>$arr['uid']))->find();
							if($re['username'] != ''){
								$arr['username'] = $re['username'];
							}else if($re['mobile'] != ''){
								$arr['username'] = $re['mobile'];
							}else{
								$arr['username'] = $re['email'];
							}
						}
						session('user_auth',$arr);
					  	$msg="登陆成功!";
					  	$this->success($msg,U('member/index'));
					}

				} else {
					$this->error($Member->getError());
				}

			} else { //登录失败
				switch($uid) {
					case -1: $error = "用户不存在或被禁用！"; break; //系统级别禁用
					case -2: $error = "密码错误！"; break;
					default: $error = "未知错误！"; break; // 0-接口参数错误（调试阶段使用）
				}
				$this->error($error);
			}

		} else {
		    $this->meta_title = '会员登录';
			//$this->display('signin/login');
			$this->display();
		}
	}

    public function loginfromdialog($username = "", $password = ""){
		if(IS_POST){ //登录验证
			/* 调用UC登录接口登录 */
			$username =safe_replace($username);//过滤
			$user = new UserApi;
			$uid = $user->login($username, $password);
			if(0 < $uid){ //UC登录成功
				/* 登录用户 */
				$Member = D("Member");
				if($Member->login($uid)){ //登录用户
					//TODO:跳转到登录前页面
		           $data["status"] =1;
                   $data["info"] = "登录成功";
                   $this->ajaxReturn($data);
				} else {
					$this->error($Member->getError());
				}

			} else { //登录失败
				switch($uid) {
					case -1: $error = "用户不存在或被禁用！"; break; //系统级别禁用
					case -2: $error = "密码错误！"; break;
					default: $error ="未知错误！"; break; // 0-接口参数错误（调试阶段使用）
				}
				$this->error($error);
			}

		} else { //显示登录表单
			$this->display('user/center');
		}
	}

	/* 退出登录 */
	public function logout(){
		if(is_login()){
			D("Member")->logout();
			$this->success("退出成功！");
		} else {
			$this->redirect("user/login");
		}
	}

	/* 找回密码 */
	public function request_password(){

		$this->display();
	}

	/* 邮件发送成功 */
	public function request_succeed(){

		$this->display();
	}

	/* 验证码，用于登录和注册 */
	public function verify(){
		$verify = new \Think\Verify();
		$verify->entry(1);
	}

	/**
	 * 获取用户注册错误信息
	 * @param  integer $code 错误编码
	 * @return string        错误信息
	 */
	private function showRegError($code = 0){
		switch ($code) {
			case -1:  $error = "用户名长度必须在16个字符以内！"; break;
			case -2:  $error = "用户名被禁止注册！"; break;
			case -3:  $error = "用户名被占用！"; break;
			case -4:  $error = "密码长度必须在6-30个字符之间！"; break;
			case -5:  $error = "邮箱格式不正确！"; break;
			case -6:  $error = "邮箱长度必须在1-32个字符之间！"; break;
			case -7:  $error = "邮箱被禁止注册！"; break;
			case -8:  $error = "邮箱被占用！"; break;
			case -9:  $error = "手机格式不正确！"; break;
			case -10: $error = "手机被禁止注册！"; break;
			case -11: $error = "手机号被占用！"; break;
			default:  $error = "未知错误";
		}
		return $error;
	}

    /**
     * 修改密码提交
     */
    public function profile(){
		if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
        if (IS_POST) {
            //获取参数
            $uid = is_login();
            $password   =   I("post.old");
            $repassword = I("post.repassword");
            $data["password"] = I("post.password");
            empty($password) && $this->error("请输入原密码");
            empty($data["password"]) && $this->error("请输入新密码");
            empty($repassword) && $this->error("请输入确认密码");

            if($data["password"] !== $repassword){
                $this->error("您输入的新密码与确认密码不一致");
            }

            $Api = new UserApi();
            $res = $Api->updateInfo($uid, $password, $data);
            if($res['status']){
                $this->success("修改密码成功！",U('Member/index'));
            }else{
                $this->error($res["info"]);
            }
        }

       	$user_auth = session('user_auth');
       	$username = $user_auth['username'];
       	$this->assign('username',$username);
        $this->meta_title = '修改密码';
        $this->display('User/profile');

    }

/*
#################################################################################################################
 */
    /**
     * 发送手机注册验证码
     */
    public function phoneVerify(){
		if(IS_POST){
			$post = I('post.');
			if($post['key'] == 'register'){
				$Member = M("ucenter_member");
				$where['mobile'] = $post['mobile'];
				$sltArr = $Member->where($where)->select();
				if(empty($sltArr)){  // 手机号不存在

					$verifyStr = str_shuffle('123456789123456789123456789');
					$verify = substr($verifyStr,0,4);
					$account = 'qdrghz';
					$pswd = 'QDrghz08';
					$msg = '您好,你的验证码:'.$verify.'【贴芯点】';
					import('Home.Library.GetCode');    // 加载类文件
					$code  = new \GetCode();

					$url="http://120.24.167.205/msg/HttpSendSM?account=".$account."&pswd=".$pswd."&mobile=".$post['mobile']."&msg=".$msg."&needstatus=true&product=";
					$str = $code->Code($url);
					$code = explode(',', $str);
                    $newCode = substr($code[1],0,1);

					if ($newCode == '0') {
						session('mobileVerify',$verify);
						$arr['infoMsg'] = 'success';
						$arr['msg'] = '发送成功！';
					 	$this->ajaxReturn($arr);
					}else{
						$arr['infoMsg'] = 'fail';
						$arr['msg'] = '发送失败！';
					 	$this->ajaxReturn($arr);
					}

				}else{
				    $arr['infoMsg'] = 'exists';
					$arr['msg'] = '手机号被占用！';
				 	$this->ajaxReturn($arr);
				}
			}
		}
    }

    /**
     * 发送动态密码
     */
    public function forgotCode(){
    	$mobile = I('post.mobile');
    	$key = I('post.key');
    	$Member = M("ucenter_member");
		$where['username'] = $mobile;
		$sltArr = $Member->where($where)->select();
		if(!empty($sltArr)){
			$verifyStr = str_shuffle('123456789abcdefghijklmnpqrstuvwxyz');
			$verify = substr($verifyStr,0,6);
			$account = 'qdrghz';
			$pswd = 'QDrghz08';
			$msg = '您好,你的动态密码:'.$verify.'【贴芯点】';

			import('Home.Library.GetCode');    // 加载类文件
			$code  = new \GetCode();

			$url="http://120.24.167.205/msg/HttpSendSM?account=".$account."&pswd=".$pswd."&mobile=".$mobile."&msg=".$msg."&needstatus=true&product=";
			$str = $code->Code($url);
			$code = explode(',', $str);
            $newCode = substr($code[1],0,1);
            if($newCode == '0'){
            	$user = new UserApi;
				$password = $user->forgot($verify);  // 加密后密码
	        	$upRe = $Member->where(array('username'=>$mobile))->setField('password',$password);
            	if($upRe){
            		$arr['infoMsg'] = 'success';
					$arr['msg'] = '发送成功！';
				 	$this->ajaxReturn($arr);
            	}
            }else{
            	echo 'bbbb';
            	$arr['infoMsg'] = 'fail';
				$arr['msg'] = '发送失败！';
			 	$this->ajaxReturn($arr);
            }
		}else{
			$arr['infoMsg'] = 'exists';
			$arr['msg'] = '该用户名不存在！';
		 	$this->ajaxReturn($arr);
		}
    }

    /**
     * 动态密码登录
     */
    public function forgot($username = "", $password = "", $verify = ""){
    	if(IS_POST){ //登录验证
    		$username = I('post.mobile');
    		$password = I('post.password');
            $username =safe_replace($username);//过滤

			/* 检测验证码 */
			/*if(!check_verify($verify)){
				$this->error("验证码输入错误！");
			}*/
			/* 调用UC登录接口登录 */
			$uMember = M('ucenter_member');
			if($uMember->where(array('username'=>$username))->find()){
				$type = 1;
			}else if($uMember->where(array('email'=>$username))->find()){
				$type = 2;
			}else if($uMember->where(array('mobile'=>$username))->find()){
				$type = 3;
			}else{
				$type = 4;
			}
			$user = new UserApi;
			$uid = $user->login($username, $password,$type);

            if(0 < $uid){ //UC登录成功
				/* 登录用户 */
				$Member = D("Member");
				if($Member->login($uid)){ //登录用户
					//TODO:跳转到登录前页面
					if($_POST['email']){
					   	$msg="注册成功!";
					   	$this->success($msg,U('user/login'));
					}else{
						$arr = session('user_auth');
						if($arr['username'] == ''){
							$re = $uMember->where(array('id'=>$arr['uid']))->find();
							if($re['username'] != ''){
								$arr['username'] = $re['username'];
							}else if($re['mobile'] != ''){
								$arr['username'] = $re['mobile'];
							}else{
								$arr['username'] = $re['email'];
							}
						}
						session('user_auth',$arr);
					  	$msg="登陆成功!";
					  	$this->success($msg,U('User/profile'));
					}
				} else {
					$this->error($Member->getError());
				}

			} else { //登录失败
				switch($uid) {
					case -1: $error = "用户不存在或被禁用！"; break; //系统级别禁用
					case -2: $error = "密码错误！"; break;
					default: $error = "未知错误！"; break; // 0-接口参数错误（调试阶段使用）
				}
				$this->error($error);
			}

		} else {
		    $this->meta_title = '忘记密码';
			$this->display();
		}
    }

}

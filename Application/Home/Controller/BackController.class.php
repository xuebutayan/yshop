<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------
namespace Home\Controller;
/**
 * 退货模型控制器
 */
class BackController extends HomeController {
 /* 退货申请 */
	public function index(){
		if ( !is_login() ) {
			  $this->error( "您还没有登陆",U("User/login") );
		 }
		$uid = is_login();
		if (IS_POST){
			$id= I('post.id',0,'intval');//获取id
            if(!is_numeric($id)){
		         $this->error('ID错误！');
		    }
            $info=M("shoplist")->where(array('id'=>$id,'uid'=>$uid))->find();
			$postnum=I('post.num');
            if(!is_numeric($postnum)){
		        $this->error('数量错误！');
		    }
            if($postnum<1 && $postnum>$info['num']){
		        $this->error('无效数量！');
		    }

		    $pics = I('post.pics');
		    if(!empty($pics)){
		    	$pics = substr($pics,1);
		    }
		    
			$num= $info['num'];//获取num
			$price=$info['price'];//获取价格
			$goodid=$info['goodid'];//获取goodid
			$parameters=$info['parameters'];
			 if ($postnum>$num) {
				 $this->error('超出购买数量');
			 } else {
					//保存信息到退货表
				 $back= D("backlist");
				 $back->create();//Create方法创建的数据对象是保存在内存,并没有实际写入到数据库，直到使用add或者save   方法才会真正写入数据库
				 $back->create_time=NOW_TIME;
				 $back->status=1;
				 $back->goodid=$goodid;
				 $back->total=$num*$price;
				 $back->parameters=$parameters;
				 $back->pics=$pics;
				 $back->uid=$uid;

				 //若已退货
				$existEx = $back->where(array('id'=>$id,'goodid'=>$goodid,'uid'=>$uid))->find();
				if(!empty($existEx)){
					$this->error('该商品已申请过退货',U('Back/lists'));  // 已申请过换货
				}

				 $result=$back->add();
				 addUserLog('申请退货，商品名称'.get_good_name($goodid).'参数'.$parameters, is_login())	;
				   //更改商品的售后信息
				 $data['status']=4;
				 $shop=M("shoplist");
				 $add=$shop->where("id='$id'")->save($data);
				  if ($add) {
							$this->success('提交成功',U("back/success"));
				  }else{
							$this->error('申请失败');
				  }
			 }
		}else{
			$this->meta_title = '提交退货单';
			$id= I('get.id',0,'intval');//获取id
			if(!is_numeric($id)){
		       $this->error('ID错误！');
		   }
		   $msg="Tips，提交退货单";
		   $detail=M("shoplist")->find($id);
			//获取购物清单
			$this->assign('list',$detail);
		    //获取物品id
			$this->assign('id',$id);
			$this->assign('msg',$msg);
			$this->display();
		}
	}

	/* 提交状态 */
	public function success(){
		if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
		$this->display('success');
	}

	/* 退货列表 */
	public function lists(){
		if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
		$map['uid'] = is_login();
		$map['status'] = array('gt',3);

		$shoplist=M("shoplist");
	   	$count = $shoplist->where($map)->count();
	   	$Page= new \Think\Page($count,10);

	   	$list = $shoplist->where($map)->order('id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
	    foreach($list as $n=> $val){
	    	$where['uid'] = is_login();
	    	$where['tag'] = $val['tag'];
        	$list[$n]['orderId']=M('order')->where($where)->getField('orderid');
        	$list[$n]['cover_id'] = M('document')->where(array('id'=>$val['goodid']))->getField('cover_id');
        	$backWhere['id'] = $val['id'];
        	$backWhere['goodid'] = $val['goodid'];
        	$list[$n]['back'] = M('backlist')->where($backWhere)->find();  // 'num,status,create_time,total,update_time,backname'
		}

		$Page= new \Think\Page($count,10);
		$Page->setConfig('prev','上一页');
	    $Page->setConfig('next','下一页');
	    $Page->setConfig('first','第一页');
      	$Page->setConfig('last','尾页');
        $Page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $showPage = $Page->show();
       
        $this->assign('list',$list);
        $this->assign('showPage',$showPage);
		$this->display('lists');
	}

	/* 退货详情 */
    public function detail(){
        if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
		$id= I('get.id',0,'intval');
		$oid = I('get.oid');
		$back=M("backlist");
		$list= $back->where("shopid='$id'")->find();
		$info= M("backlist")->where("shopid='$id'")->getField("backinfo");
		
		$this->assign('oid',$oid);
		$this->assign('list',$list);
		$this->assign('backinfo',$info);
		$msg="退货单";
		$this->assign('msg',$msg);
		$this->display();
    }

    /* 买家快递信息 */
    public function backkuaidi(){
    	if ( !is_login() ) {
			$this->error( "您还没有登陆",U("User/login") );
		}
		$back=M("backlist");
		$shoplist = M('shoplist');
		$where['id'] = I('post.backid');
		$data['tool'] = I('post.tool');
		$data['toolid'] = I('post.toolid');
		$data['status'] = 4;
		$reback = $back->where($where)->save($data);
		$reshoplist = $shoplist->where(array('id'=>I('post.backid')))->save(array('status'=>6));
		if($reback && $reshoplist){
			$this->success('提交成功');
		}else{
			$this->error('提交失败');
		}
    }
    
    /* 发货 */
    public function send(){
    	if ( !is_login() ) {
    		$this->error( "您还没有登陆",U("User/login") );
    	}
    	$back = M('backlist');
    	$id= I('get.id',0,'intval');
    	$back=M("backlist");
    	$list= $back->where("shopid='$id'")->find();
    	
    	$this->assign('list',$list);
    	$this->display();
    }

}

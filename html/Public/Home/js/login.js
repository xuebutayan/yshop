﻿/*验证登录*/
function login(){
	var key = 'login';
	var user_name = $('#user_name').val();
	var password = $('#password').val();
	$.post('index.php?m=login&a=verify',{key:key,user_name:user_name,password:password},function(msg){
		if (msg.infoMsg == 'user_name') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 'password') {
			alert(msg.errorMsg);
		};
		/*if (msg.infoMsg == 'succeed') {
			window.location.href= document.referrer;  //返回上一页面
           
		};*/
		if (msg.infoMsg == 'succeed') {
			if(document.referrer == 'http://www.51tiexin.com/login-retrieve.html'){
				window.location.href = '/goods-index.html';
			}else{
				window.location.href= document.referrer;  //返回上一页面
			}
			return false;
			
		};

	},'JSON');	
}

/*注册发送验证手机验证码*/
function codes(){
	var key = 'register';
	var phone = $('#phone').val();
    $.post('index.php?m=login&a=Code',{key:key,phone:phone},function(msg){
    	if (msg.infoMsg == 'noPhone') {
			alert(msg.errorMsg);
		};
    	if (msg.infoMsg == 'errPhone') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'isPhone') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'failure') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'succeed') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'sendFailure') {
    		alert(msg.errorMsg);
    	};
    },'JSON');  
}

/*注册验证*/
function register(){
	var key = 'register';
	var state = $.trim($('input[name="state"]:checked').val());
	var phone = $.trim($('#phone').val());
	var password = $.trim($('#password').val());
	var re_password = $.trim($('#re_password').val());
	var code = $.trim($('#code').val());

	$.post('index.php?m=login&a=doRegister',{key:key,state:state,phone:phone,password:password,re_password:re_password,code:code},function(msg){
		if (msg.infoMsg == 'noPhone') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 'phone') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 'bePhone') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 'password') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 're_password') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 'code') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 'state') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 'succeed') {
    		window.location.href="/center-info.html";
    	};
	},'JSON');
}
/*找回密码发送手机验证码*/
function recodes(){
	var key = 'callblack';
	var phone = $('#phone').val();
	$.post('index.php?m=login&a=reCode',{key:key,phone:phone},function(msg){
		if (msg.infoMsg == 'phone') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'errPhone') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'noPhone') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'failure') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'succeed') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'sendFailure') {
    		alert(msg.errorMsg);
    	};
	},'JSON');
}
/*验证找回密码*/
function next(){
	var key = 'next';
	var phone = $('#phone').val();
	var code = $('.code').val();
	$.post('index.php?m=login&a=doRetrieve',{key:key,code:code,phone:phone},function(msg){
		if (msg.infoMsg == 'phone') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'code') {
    		alert(msg.errorMsg);
    	};
    	if (msg.infoMsg == 'succeed') {
    		var black = document.getElementById('block');
    		var key_word = document.getElementById('key_word');
    		black.style.display = "none";
    		$('input[name=phone]').val(phone);
    		key_word.style.display = "block";
    	};
	},'JSON');
}
/*找回修改密码*/
function callKeyword(){
	var key = 'pass_word';
	var phone = $('input[name=phone]').val();
	var password = $('input[name=password]').val();
	var re_password = $('input[name=re_password]').val();
	$.post('index.php?m=login&a=keyword',{key:key,phone:phone,password:password,re_password:re_password},function(msg){
		//alert(msg);
		if (msg.infoMsg == 'ispass') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 'password') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 're_password') {
			alert(msg.errorMsg);
		};
		if (msg.infoMsg == 'succeed') {
			alert(msg.errorMsg);
			window.location.href="/login-login.html";
		};
		if (msg.infoMsg == 'failure') {
			alert(msg.errorMsg);
			window.location.href="/login-retrieve.html";
		};
	},'JSON');
}